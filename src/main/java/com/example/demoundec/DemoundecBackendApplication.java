package com.example.demoundec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoundecBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoundecBackendApplication.class, args);
    }

    @GetMapping(value = "/saludo")
    public String saludar() {
        return "Hola mundo!";
    }
}
